# Datrans Website v2.0

## Требования:
  * Node >= 0.12.x ([nodejs.org](https://nodejs.org/))
  * npm >=2.11.x (`npm install -g npm@latest`)
  * php >= 5.4.0 or [short_open_tag](http://php.net/manual/de/ini.core.php#ini.short-open-tag) set `true` on your VM/Webserver

## Запуск
#### Workflow
Тема используется gulp для сборки scss, js в папку `distr`. Детали команд в файле `gulpfile.js`.
#### Установка gulp и зависимостей:
  * Установить gulp и Bower глобально командой `npm install -g gulp bower` если еще не установлено
  * В папке с темой запустить `npm install && bower install && gulp`
  * Добавить domain/ip to `browsersync_proxy` in `gulpfile.js`

#### Запуск gulp
  * Запустить команду `gulp watch` в директории с темой `/wp-content/themes/datrans/` для автоматической сборки .scss, .js перезагрузки браузера при изменении файлов темы


