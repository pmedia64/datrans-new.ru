<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'fh7915ec_dtr_new');

/** Имя пользователя MySQL */
define('DB_USER', 'fh7915ec_dtr_new');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '%*?ugg9s');

/** Имя сервера MySQL */
define('DB_HOST', 'fh7915ec.beget.tech');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@fkDEEz9DXDtBSZ)Dc{91utoI2h:Z-P#.UcXI1W6H`Aa~NnLRD[sFr^G*z{eMI}A');
define('SECURE_AUTH_KEY',  'mI+st/_C-Vsd<DuRz5,O!;G<sG^#>4E[.Y$*$%ZBo.yW+&^@bEI56)@FN HAl-sd');
define('LOGGED_IN_KEY',    '@-7=fBJX}Ofu00Q~Ny=}6wE8(K56pQIj;3jC(<, hMU_,3-r`lDL8*rOF|%B@ax:');
define('NONCE_KEY',        ']1)E9iW,]u]=OpNCb*.Fh6ovg:/T/NipEE$qRd3Y<M[SEfSi p*k&g{=3Lg##OUG');
define('AUTH_SALT',        '^sQBs]wFu>[4SaTz?V3ta={CL8o~shc?5Fd6+%Jocl(aYUb#^M]&I<z,=#gOCca}');
define('SECURE_AUTH_SALT', 'A&.r#R%?%7fHBSk*-01983KKWS32x){)]F0$FnwgbsDW<kelyo)$j).>$V+gn^.$');
define('LOGGED_IN_SALT',   'q1p,qm,c;oH`*tP1mP]P<oW&L4-8_.8Ku Z)k)g(~MR!kc/Od!Zmdj:PEG_+>yHD');
define('NONCE_SALT',       '(Q05m$E$}_c;/G~(hQ%rc`1-v5{GSAz!(?V}>nAy[Cq(Q`<i-@ `i__L#fBk>(lF');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'dtrs_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Multisite */
define( 'WP_ALLOW_MULTISITE', true );
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', true);
define('DOMAIN_CURRENT_SITE', 'datrans-new.ru');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
