<?php

/** Вернуть абсолютную ссылку на папку Icon-png
 * @return string
 */

function get_iconPNG($iconName) {
  return get_template_directory_uri().'/assets/images/icon-png/'.$iconName.'.png';
}

//Add acf option page
if (function_exists('acf_add_options_page')) {

  acf_add_options_page(array(
    'page_title' => 'Top menu',
    'menu_title' => 'Tom menu Option',
    'menu_slug' => 'option-settings',
    'capability' => 'top-menu',
    'redirect' => false
  ));
  
}
