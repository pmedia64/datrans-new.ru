<?php

/*для проверки ролей пользователя*/
function appthemes_check_user_role($role, $user_id = null)
{

  if (is_numeric($user_id))
    $user = get_userdata($user_id);
  else
    $user = wp_get_current_user();

  if (empty($user))
    return false;

  return in_array($role, (array)$user->roles[0]);
}

if (appthemes_check_user_role('administrator')) { //для безопасности
  require get_template_directory() . '/crm/src/city-list/service.php';
  require get_template_directory() . '/crm/src/import-price/service.php';
  require get_template_directory() . '/crm/src/city-price/service.php';
}
require get_template_directory() . '/crm/src/key-log/service.php';