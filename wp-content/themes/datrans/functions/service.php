<?php
/**
 * Подключение к БД с прайсами
 * @return wpdb
 */
function connect_to_price_db(){
    return new wpdb('fh7915ec_prices','hjl;th','fh7915ec_prices','fh7915ec.beget.tech');
}

/**
 * Названия таблиц в БД с прайсами
 * @return object
 */
function get_price_table_names(){
    return (object) array(
        'city_list'=>'dtr_city_list',
        'city_keylog'=>'dtr_city_keylog',
        'city_price'=>'dtr_city_price',
    );
}

function return_price_search($start = null, $fin = null)
{
  $price_db = connect_to_price_db();
  $actualKeys = return_actual_keys();
  $cityFrom = trim($start) ? trim($start) : trim($_GET['cityFrom']);
  $cityTo = trim($fin) ? trim($fin) : trim($_GET['cityTo']);
  $withKeys = trim($_GET['withKeys']) ?: true;

  if ($cityFrom && $cityTo) {

    $res = $price_db->get_results(
      "SELECT cityFrom, cityTo, field1, field2, field3, field4
	FROM ".get_price_table_names()->city_price."
	WHERE cityFrom LIKE '$cityFrom%'
	AND cityTo LIKE '$cityTo%'
	LIMIT 2
	");
    $mskK = 1;
    if (strtolower($cityTo) == 'москва') $mskK = 1.15; // TODO
    if ($withKeys && $res[0]) {
      $res[0]->field1 = (int)(preg_replace("/[^0-9]/", "", $res[0]->field1) * $actualKeys->key1 * $mskK);
      $res[0]->field2 = (int)(preg_replace("/[^0-9]/", "", $res[0]->field2) * $actualKeys->key2 * $mskK);
      $res[0]->field3 = (int)(preg_replace("/[^0-9]/", "", $res[0]->field3) * $actualKeys->key3 * $mskK);
      $res[0]->field4 = (int)(preg_replace("/[^0-9]/", "", $res[0]->field4) * $actualKeys->key4 * $mskK);
    }

  } else {

  }
  if ($res[0]) {
    return $res;
  } else {
    return false;
  }

}

function get_price_search()
{
  $res = return_price_search();
  $encoded = json_encode($res[0], true);
  wp_die($encoded);
}

add_action('wp_ajax_get_price_search', 'get_price_search');    // If called from admin panel
add_action('wp_ajax_nopriv_get_price_search', 'get_price_search');    // If called from front end


function search_city_list()
{
  global $wpdb;
  $q = trim($_GET['q']);

  //исключения
  if ($q == 'спб' || $q == 'пит' || $q == 'пет') {
    $q = 'Санкт-Петербург';
  }
  if ($q == 'рнд') {
    $q = 'Ростов-на-Дону';
  }
  if ($q == 'мск') {
    $q = 'Москва';
  }
  if ($q == 'нн') {
    $q = 'Нижний Новгород';
  }
  if ($q == 'екб') {
    $q = 'Екатеринбург';
  }
  $response = array();

  $res = $wpdb->get_results(
    "
	SELECT name
	FROM ".get_price_table_names()->city_list."
	WHERE name LIKE '$q%'
	LIMIT 5
	"
  );

  if (count($res) > 0) {
    array_push($response, $res);
  } else {
//        http_response_code(404);
//        wp_die(json_encode(array('message' => 'ERROR', 'code' => 500)));
  }

  $encoded = json_encode($response[0], 256);
  echo($encoded);
  wp_die();
}

add_action('wp_ajax_search_city_list', 'search_city_list');    // If called from admin panel
add_action('wp_ajax_nopriv_search_city_list', 'search_city_list');    // If called from front end



function get_default_city_list()
{
  $price_db = connect_to_price_db();

  $response = $price_db->get_results(
    "
	SELECT name
	FROM " . get_price_table_names()->city_list . "
	WHERE defaultCity = 1 
	"
  );

  return $response;
}

add_action('wp_ajax_get_default_city_list', 'get_default_city_list');    // If called from admin panel
add_action('wp_ajax_nopriv_get_default_city_list', 'get_default_city_list');    // If called from front end


function get_default_city_grid($cityFrom)
{
  $price_db = connect_to_price_db();
  $actualKeys = return_actual_keys();

  $defaultCity = get_default_city_list();
  $sql = array();
  foreach ($defaultCity as $city) {
    $sql[] = '`cityTo` LIKE "' . $city->name . '"';
  }

  $query = 'SELECT cityTo, field1, field2, field3, field4
  FROM '.get_price_table_names()->city_price.'
  WHERE `cityFrom` = "' . $cityFrom . '"
  AND ( ' . implode(' OR ', $sql) . ')';

  $res = $price_db->get_results($query);

  if (count($res) > 0) {
    foreach ($res as $item) {
      $item->field1 = (int)(preg_replace("/[^0-9]/", "", $item->field1) * $actualKeys->key1);
      $item->field2 = (int)(preg_replace("/[^0-9]/", "", $item->field2) * $actualKeys->key2);
      $item->field3 = (int)(preg_replace("/[^0-9]/", "", $item->field3) * $actualKeys->key3);
      $item->field4 = (int)(preg_replace("/[^0-9]/", "", $item->field4)* $actualKeys->key4);
    }
  }

  return $res;
  wp_die();
}

add_action('wp_ajax_get_default_city_grid', 'get_default_city_grid');    // If called from admin panel
add_action('wp_ajax_nopriv_get_default_city_grid', 'get_default_city_grid');    // If called from front end
