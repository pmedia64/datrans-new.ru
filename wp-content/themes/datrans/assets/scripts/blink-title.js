blinkTitle("ДА-ТРАНС", "БЫСТРО!", "ВЫГОДНО!", "НАДЕЖНО!", 1500);
function blinkTitle() {
  var msg = [], i = 0;
  while (i < arguments.length && typeof arguments[i] === 'string') msg.push(arguments[i++]);
  var delay = arguments[i++] || 1000,
    isFocus = arguments[i++] || false,
    timeout = arguments[i++] || false;

  i = 0;
  function nextTitle() {
    document.title = msg[i++ % msg.length]
  }

  if (timeout) {
    setTimeout(blinkTitleStop, timeout);
  }

  nextTitle();

  if (isFocus) {
    var onPage = false;
    var testflag = true;
    var initialTitle = document.title;
    window.onfocus = function () {
      onPage = true;
    };
    window.onblur = function () {
      onPage = false;
      testflag = false;
    };
    blinkTitle.hold = window.setInterval(function () {
      if (!onPage) nextTitle();
    }, delay);
  } else {
    blinkTitle.hold = window.setInterval(nextTitle, delay);
  }
}

function blinkTitleStop() {
  clearInterval(blinkTitle.hold);
}
