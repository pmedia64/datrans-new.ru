<div class="col-xs-3 col-lg-3 logo">
  <a href="/">
    <span class="logo">
      <img src="<?php echo get_iconPNG('logo') ?>">
    </span>
  </a>
  <span class="logo-otp">Город отправки:</span>
    <span class="region">
        <a href="#" data-toggle="modal" id="#show_region" data-target=".pop-up-2">
            <?php echo get_bloginfo('name'); ?>
          <span class="region_popover" data-container="body" data-toggle="popover" data-placement="bottom"></span>
        </a>
    </span>
</div>

<script>
  $(document).ready(function () {
    $('[data-toggle="popover"]').popover({
      'placement': 'bottom', 'html': true,
      'content': 'Город отправки: <?php echo get_bloginfo('name'); ?><br> <a class="btn btn-sm" id="popover_close">Да</a> <a class="btn btn-sm" id="nomycity">Нет</a>'
    });
    var cookie_city = $.cookie('cookie_city');
    console.log(cookie_city);
    if (cookie_city == 'null' || !cookie_city) {
      $('[data-toggle="popover"]').popover('show');
    }

    $('[data-target=".pop-up-2"]').click(function () {
      $('[data-toggle="popover"]').popover('hide')
    });

    $('#popover_close').click(function () {
      $('[data-toggle="popover"]').popover('hide')
      $.cookie('cookie_city', '<?php echo get_bloginfo('name'); ?>');
    });

    $('#nomycity').click(function () {
      $('[data-toggle="popover"]').popover('hide')
      $('.pop-up-2').modal('show');
      $.cookie('cookie_city', null);
    });
  });
</script>