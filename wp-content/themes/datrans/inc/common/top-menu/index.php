<aside id="pre_nav">
  <div class="container">

    <?php require 'logos.php'; ?>
    <nav class="nav">
      <ul class="nav__menu ">
        <?php if (have_rows('fl-topmenu-wrapper', 'option')):
          while (have_rows('fl-topmenu-wrapper', 'option')) : the_row(); ?>

            <div class="col-cs">
              <li class="nav__menu-item">
                <a href="<?php the_sub_field('fl-topmenu-url', 'option'); ?>">
                  <?php the_sub_field('fl-topmenu-title', 'option'); ?>
                </a>

                <?php if (have_rows('fl-topmenu-subtitle-wrrapper', 'option')): ?>
                  <ul class="nav__submenu">
                    <?php while (have_rows('fl-topmenu-subtitle-wrrapper', 'option')) : the_row(); ?>
                      <li class="nav__submenu-item">
                        <a href="<?php the_sub_field('fl-topmenu-subtitle-url', 'option'); ?>"
                           class="sub-nav"><?php the_sub_field('fl-topmenu-subtitle', 'option'); ?>
                        </a>
                      </li>
                    <?php endwhile; ?>
                  </ul>
                <?php endif; ?>
                
              </li>
            </div>
          <?php endwhile;
        endif; ?>
      </ul>
    </nav>
    <?php require 'phones.php' ?>
  </div>
</aside>