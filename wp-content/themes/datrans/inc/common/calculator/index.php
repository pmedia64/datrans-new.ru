<?php
$category = get_the_category();
$parent_cat = $category[0]->category_parent;
?>

<div class="form-online">
  <a name="raschet"></a>
  <form role="form" action="/calc#raschet" method="post">
    <h3>Online-расчет стоимости перевозки</h3>

    <div class="input-group">
      <input id="from" class="typeahead form-control" type="text" name="start" value="<?php echo trim($_POST['start']) ?>"
             placeholder="Откуда">
    </div>
    <?php if (get_page_template_slug()=='template-calc.php') {?>
      <div class="swap_button"></div>
    <?php } ?>
    <div class="input-group">
      <input id="fin" class="form-control" type="text" name="fin" value="<?php echo trim($_POST['fin']) ?>" placeholder="Куда">
    </div>

    <div class="input-group">
      <input id="mass" type="text" class="form-control" name="mass" value="<?php echo trim($_POST['mass']) ?>" placeholder="Вес, кг">
    </div>
    <div class="input-group">
      <button type="submit" id="calc_submit" class="btn" onclick="yaCounter25094516.reachGoal('raschet'); return true;">Рассчитать</button>
    </div>
  </form>
</div>