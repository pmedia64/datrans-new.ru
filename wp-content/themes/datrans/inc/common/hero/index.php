<div class="hero">

  <div class="container">
    <div class="hero__title-wrapper" style="padding-right: 15px;">
      <?php $parent_cat = $category[0]->category_parent; ?>
      <?php
      $title_text = '';
      if ($parent_cat == '13') {
        $title_text = '<p class="hero__title ">Грузоперевозки по России и СНГ</p><p class="hero__title2">Беларусь, Казахстан, Армения, Абхазия</p>';
        echo $title_text;
      } else if ($parent_cat == '25') {
        $title_text = '<p class="hero__title">Грузоперевозки в Крым и из Крыма</p><p class="hero__title2">Огромный опыт перевозок по этому направлению!</p>';
        echo $title_text;
      } else echo '<p class="hero__title">Грузоперевозки в любую точку России</p><p class="hero__title2">Возите грузы с нами! Это удобно и безопасно</p>';
      ?>
    </div>
  </div>

  <div class="overflow">
    <div class="container">
      <?php
      if (get_the_ID() != 164) { ?>
        <?php //форма калькулятора
        require get_template_directory() . '/inc/common/calculator/index.php'; ?>
      <?php } ?>
    </div>
  </div>

</div>