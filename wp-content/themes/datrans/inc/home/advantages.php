<div class="advantages clearfix">

    <div class="view view-second">
      <img src="<?php echo get_iconPNG('geo') ?>" width="130px"/>
      <span>Широкая география <br>грузоперевозок</span>
      <div class="mask"></div>
      <div class="content">
        <h2>География грузоперевозок</h2>
        <p>Все регионы России, Белоруссия, Казахстан, Армения, Абхазия. Включая сложные и непопулярные
          направления.</p>
        <a href="/gruzoperevozki_po_rossii" class="info">Подробнее</a>
      </div>
    </div>

    <div class="view view-second">
      <img src="<?php echo get_iconPNG('strahovka') ?>" width="130px"/>
      <span>Бесплатное страхование<br>от всех рисков</span>
      <div class="mask"></div>
      <div class="content">
        <h2>Страхование грузов от всех рисков</h2>
        <p><br>Грузы стоимостью до 1,5 млн. руб. страхуются за наш счет.</p>
        <a href="/strahovanie-gruzov-pri-perevozke" class="info">Подробнее</a>
      </div>
    </div>

    <div class="view view-second">
      <img src="<?php echo get_iconPNG('servis') ?>" width="130px"/>
      <span>Высокие стандарты сервиса<br>для всех клиентов</span>
      <div class="mask"></div>
      <div class="content">
        <h2>Наши стандарты обслуживания</h2>
        <p>Быстрый расчет стоимости, личный менеджер 24/7, смс-информирование, отлаженный документооборот.</p>
        <a href="#ed-standart" class="info">Подробнее</a>
      </div>
    </div>

    <div class="view view-second">
      <img src="<?php echo get_iconPNG('online') ?>" width="130px"/>
      <span>Online отслеживание <br>грузов на сайте</span>
      <div class="mask"></div>
      <div class="content">
        <h2>Online отслеживание грузов</h2>
        <p>Удобный онлайн-сервис позволяет нашим Клиентам контролировать передвижение груза.</p>
        <a href="/otsledit-gruz" class="info">Подробнее</a>
      </div>
    </div>

</div>

<div class="otziv-zag">
  <div class="headline">КОМПАНИЯ ДА-ТРАНС</div>
  <div class="hd-text"></div>
</div>