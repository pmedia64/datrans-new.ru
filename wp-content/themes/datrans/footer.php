<?
/**
 * @author      Flurin Dürst
 * @version     1.7
 * @since       WPSeed 0.1
 */
?>

    <? include 'templates/str-footer.php' ?>

    <? wp_footer() ?>

<!-- hot reload -->
<script id="__bs_script__">//<![CDATA[
  document.write("<script async src='http://HOST:3060/browser-sync/browser-sync-client.js?v=2.18.13'><\/script>".replace("HOST", location.hostname));
  //]]></script>
  </body>
</html>
