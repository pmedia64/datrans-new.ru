var app = angular.module('app', ['ui.router', 'ngTable', 'ngResource', 'papa-promise', 'underscore', 'ngSanitize', 'ui.bootstrap', 'ngTableExport']);

app.config(function ($stateProvider, $urlRouterProvider) {
    //
    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise("/city-list");
    //
    // Now set up the states
    $stateProvider
        .state('city-list', {
            url: "/city-list",
            templateUrl: "/wp-content/themes/datrans/crm/src/city-list/index.html"
        })
        .state('city-price', {
            url: "/city-price/:id",
            templateUrl: "/wp-content/themes/datrans/crm/src/city-price/index.html"
        })
        .state('import-price', {
            url: "/import-price",
            templateUrl: "/wp-content/themes/datrans/crm/src/import-price/index.html"
        })
        .state('key-log', {
            url: "/key-log",
            templateUrl: "/wp-content/themes/datrans/crm/src/key-log/index.html"
        });
});

angular
    .module('app')
    .constant('myConfig', {
        themeUrl: 'http://datrans-new.ru/wp-content/themes/datrans/',
        baseUrl: 'http://datrans-new.ru/',
        ajaxUrl: 'http://datrans-new.ru/wp-admin/admin-ajax.php'
    }).constant('_', window._)
    .run();


var underscore = angular.module('underscore', []);
underscore.factory('_', ['$window', function($window) {
    return $window._; // assumes underscore has already been loaded on the page
}]);