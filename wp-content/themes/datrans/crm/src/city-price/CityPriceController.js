var app;
(function (app) {
    var CityPriceController = (function () {
        function CityPriceController($scope, NgTableParams, $resource, myConfig, CityPriceService, ImportPriceService, $rootScope, $state, $stateParams) {
            this.$scope = $scope;
            this.NgTableParams = NgTableParams;
            this.$resource = $resource;
            this.myConfig = myConfig;
            this.CityPriceService = CityPriceService;
            this.ImportPriceService = ImportPriceService;
            this.$rootScope = $rootScope;
            this.$state = $state;
            this.$stateParams = $stateParams;
            this.removeList = [];
            this.loading = {
                load: false,
                remove: false
            };
            this.isEditing = false;
            this.error = null;
            this.withKeys = true;
            this.checkboxes = {
                'checked': false,
                items: {}
            };
            this.$scope.vm = this;
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
            this.init();
        }
        CityPriceController.prototype.init = function () {
            this.getCityPrice();
        };
        CityPriceController.prototype.getCityPrice = function () {
            var _this = this;
            this.loading.load = true;
            this.price = [];
            this.CityPriceService.getPrice(this.$rootScope.$stateParams.id, this.withKeys).then(function (res) {
                _this.loading.load = false;
                _this.price = res;
                if (_this.tableParams != 'undefined') {
                    _this.tableParams = new _this.NgTableParams({ page: 1, count: 10000 }, { dataset: _this.price });
                }
                else {
                    _this.tableParams.reload();
                }
                //Проверить на дубль
                var dupArr = [];
                var groupedByCount = _.countBy(_this.price, function (item) {
                    return item.cityTo;
                });
                for (var name in groupedByCount) {
                    if (groupedByCount[name] > 1) {
                        _.where(_this.price, {
                            cityTo: name
                        }).map(function (item) {
                            dupArr.push(item);
                            item.error = 'double';
                            _this.error = 'Найдены дубли в городах <br>';
                        });
                    }
                }
                console.log(dupArr);
            }).catch(function (err) {
            }).finally(function () {
                _this.loading.load = false;
            });
        };
        CityPriceController.prototype.del = function () {
            var _this = this;
            var isYes = confirm("Будут удалены прайсы выбранных городов");
            if (isYes) {
                this.loading.remove = true;
                _.forEach(this.price, function (item) {
                    _.forEach(_this.checkboxes.items, function (checkbox, key) {
                        if (item.id == key && checkbox == true)
                            _this.removeList.push(item);
                    });
                });
                this.CityPriceService.removeSubtrace(this.removeList).then(function (res) {
                    _this.getCityPrice();
                    _this.checkboxes.items = {};
                    _this.loading.remove = false;
                    console.log('Удалены: ', res);
                }).catch(function (err) {
                    _this.loading.remove = false;
                });
            }
            // console.log(removePriceArr);
            // this.price = _.without(this.price, _.findWhere(this.price, {cityTo: item.cityTo}));
            // this.tableParams.dataset = this.price;
            // this.tableParams = new this.NgTableParams({page: 1, count: 10000}, {dataset: this.price});
            // this.removeList.push(item);
            // console.log(this.removeList);
        };
        CityPriceController.prototype.edit = function () {
            this.isEditing = true;
        };
        CityPriceController.prototype.save = function () {
            var _this = this;
            _.chunk = function (array, chunkSize) {
                return _.reduce(array, function (reducer, item, index) {
                    reducer.current.push(item);
                    if (reducer.current.length === chunkSize || index + 1 === array.length) {
                        reducer.chunks.push(reducer.current);
                        reducer.current = [];
                    }
                    return reducer;
                }, { current: [], chunks: [] }).chunks;
            };
            var chunked = _.chunk(this.price, 100);
            var x = 0;
            _.forEach(chunked, function (item) {
                _this.ImportPriceService.setCityPrice(item, true).then(function (res) {
                    x++;
                }).catch(function (err) {
                });
            });
            if (this.removeList) {
                this.CityPriceService.removeSubtrace(this.removeList).then(function (res) {
                    console.log('Удалены: ', res);
                }).catch(function (err) {
                });
            }
        };
        CityPriceController.prototype.add = function () {
            this.tableParams.settings().dataset.unshift({
                cityFrom: "",
                cityTo: null,
                field1: null,
                field2: null,
                field3: null,
                field4: null
            });
        };
        CityPriceController.$inject = ['$scope', 'NgTableParams', '$resource', 'myConfig', 'CityPriceService', 'ImportPriceService', '$rootScope', '$state', '$stateParams'];
        return CityPriceController;
    }());
    app.CityPriceController = CityPriceController;
    angular.module('app').controller('CityPriceController', app.CityPriceController);
})(app || (app = {}));
//# sourceMappingURL=CityPriceController.js.map