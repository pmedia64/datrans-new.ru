var app;
(function (app) {
    var Services;
    (function (Services) {
        var CityPriceService = (function () {
            function CityPriceService($q, $http, myConfig) {
                this.$q = $q;
                this.$http = $http;
                this.myConfig = myConfig;
            }
            CityPriceService.prototype.getPrice = function (cityId, withKeys) {
                var deferred = this.$q.defer();
                this.$http({
                    method: 'GET',
                    url: this.myConfig.ajaxUrl,
                    params: { action: 'get_price', cityId: cityId, withKeys: withKeys }
                }).success(function (res) {
                    deferred.resolve(res);
                }).error(function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };
            CityPriceService.prototype.removeSubtrace = function (query) {
                var deferred = this.$q.defer();
                this.$http({
                    method: 'POST',
                    url: this.myConfig.ajaxUrl,
                    params: { action: 'remove_subtrace' },
                    data: query
                }).success(function (res) {
                    deferred.resolve(res);
                }).error(function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };
            CityPriceService.$inject = ['$q', '$http', 'myConfig'];
            return CityPriceService;
        }());
        Services.CityPriceService = CityPriceService;
    })(Services = app.Services || (app.Services = {}));
    angular.module("app").service('CityPriceService', app.Services.CityPriceService);
})(app || (app = {}));
//# sourceMappingURL=CityPriceService.js.map