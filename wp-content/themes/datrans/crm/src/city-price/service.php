<?php

/**
 * Список городов POST
 */
function get_price()
{
    $price_db = connect_to_price_db();
    $actualKeys = return_actual_keys();
    $cityId = trim($_GET['cityId']) ?: null;
    $withKeys = trim($_GET['withKeys']) ? trim($_GET['withKeys']) : 'true';
    $response = array();

    if ($cityId) {

        $name = $price_db->get_results(
            "
	SELECT name
	FROM " . get_price_table_names()->city_list . "
	WHERE id = '$cityId'
	"
        );

        $name = $name[0]->name;

        $res = $price_db->get_results(
            "
	SELECT *
	FROM " . get_price_table_names()->city_price . "
	WHERE cityFrom = '$name'
	"
        );

        if ($withKeys == 'true') {
            foreach ($res as $item) {
                $item->field1 = (int)(preg_replace("/[^0-9]/", "", $item->field1) * $actualKeys->key1);
                $item->field2 = (int)(preg_replace("/[^0-9]/", "", $item->field2) * $actualKeys->key2);
                $item->field3 = (int)(preg_replace("/[^0-9]/", "", $item->field3) * $actualKeys->key3);
                $item->field4 = (int)(preg_replace("/[^0-9]/", "", $item->field4) * $actualKeys->key4);
                $item->defaultCity = $item->defaultCity == 1 ? true : false;
            }
        }
        array_push($response, $res);

    } else {

    }


    $encoded = json_encode($response[0], TRUE);
    print_r($encoded);
    wp_die();

}

add_action('wp_ajax_get_price', 'get_price');    // If called from admin panel
add_action('wp_ajax_nopriv_get_price', 'get_price');    // If called from front end


/**
 * Удалить под направление PUT
 */
function remove_subtrace()
{
  $price_db = connect_to_price_db();
    $request_body = file_get_contents('php://input');

    $data = json_decode($request_body, true);

    $response = array();

    foreach ($data as $item) {
        $cityFrom = $item['cityFrom'];
        $cityTo = $item['cityTo'];
        $id = $item['id'];

        $res = $price_db->query(
          $price_db->prepare(
                "DELETE FROM " . get_price_table_names()->city_price . "
		 WHERE id = %d",
                $id
            )
        );

        $item = array(
            'result' => $res,
            'id' => $id,
            'cityFrom' => $cityFrom,
            'cityTo' => $cityTo
        );

        array_push($response, $item);
    }

    $encoded = json_encode($response, TRUE);
    print_r($encoded);
    wp_die();


}

add_action('wp_ajax_remove_subtrace', 'remove_subtrace');    // If called from admin panel
add_action('wp_ajax_nopriv_remove_subtrace', 'remove_subtrace');    // If called from front end
