module app {
    declare var angular;
    declare var _;

    export module Services {

        export class CityPriceService {
            static $inject = ['$q', '$http', 'myConfig'];

            constructor(private $q,
                        private $http,
                        private myConfig) {
            }

            public getPrice(cityId, withKeys) {
                var deferred = this.$q.defer();
                this.$http({
                    method: 'GET',
                    url: this.myConfig.ajaxUrl,
                    params: {action: 'get_price', cityId: cityId, withKeys}
                }).success(function (res:any) {
                    deferred.resolve(res);
                }).error((error)=> {
                    deferred.reject(error);
                });
                return deferred.promise;
            }

            public removeSubtrace(query) {
                var deferred = this.$q.defer();
                this.$http({
                    method: 'POST',
                    url: this.myConfig.ajaxUrl,
                    params: {action: 'remove_subtrace'},
                    data: query
                }).success(function (res:any) {
                    deferred.resolve(res);
                }).error((error)=> {
                    deferred.reject(error);
                });
                return deferred.promise;
            }

        }
    }
    angular.module("app").service('CityPriceService', app.Services.CityPriceService);
}