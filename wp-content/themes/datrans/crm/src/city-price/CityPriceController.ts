module app {

    declare var angular;
    declare var _;

    export class CityPriceController {

        static $inject = ['$scope', 'NgTableParams', '$resource', 'myConfig', 'CityPriceService', 'ImportPriceService', '$rootScope', '$state', '$stateParams'];
        public isLoading:boolean;
        private tableParams;
        public price;
        public removeList = [];
        public loading =
        {
            load: false,
            remove: false
        };
        public isEditing = false;
        public error = null;
        public withKeys = true;
        public checkboxes={
            'checked': false,
            items: {}
        };

        constructor(private $scope,
                    private NgTableParams,
                    private $resource,
                    private myConfig,
                    private CityPriceService,
                    private ImportPriceService,
                    private $rootScope,
                    private $state,
                    private $stateParams) {
            this.$scope.vm = this;
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
            this.init();
        }

        public init() {
            this.getCityPrice();
        }

        private getCityPrice() {
            this.loading.load = true;
            this.price = [];
            this.CityPriceService.getPrice(this.$rootScope.$stateParams.id, this.withKeys).then(res=> {
                this.loading.load = false;
                this.price = res;
                if (this.tableParams != 'undefined') {
                    this.tableParams = new this.NgTableParams({page: 1, count: 10000}, {dataset: this.price});
                } else {
                    this.tableParams.reload();
                }

                //Проверить на дубль
                var dupArr = [];
                var groupedByCount = _.countBy(this.price, function (item) {
                    return item.cityTo;
                });

                for (var name in groupedByCount) {
                    if (groupedByCount[name] > 1) {
                        _.where(this.price, {
                            cityTo: name
                        }).map(  (item) =>{
                            dupArr.push(item);
                            item.error='double';
                            this.error='Найдены дубли в городах <br>';
                        });
                    }
                }
                console.log(dupArr);
            }).catch(err=> {

            }).finally(()=> {
                this.loading.load = false;
            });
        }

        private del() {
            var isYes = confirm("Будут удалены прайсы выбранных городов");

            if (isYes) {
                this.loading.remove = true;
                _.forEach(this.price, item=> {
                    _.forEach(this.checkboxes.items, (checkbox, key)=> {
                        if (item.id == key && checkbox == true)
                            this.removeList.push(item)
                    });
                });

                this.CityPriceService.removeSubtrace(this.removeList).then(res=> {
                    this.getCityPrice();
                    this.checkboxes.items = {};
                    this.loading.remove = false;
                    console.log('Удалены: ', res)
                }).catch(err=> {
                    this.loading.remove = false;
                });

            }
            // console.log(removePriceArr);
            // this.price = _.without(this.price, _.findWhere(this.price, {cityTo: item.cityTo}));
            // this.tableParams.dataset = this.price;
            // this.tableParams = new this.NgTableParams({page: 1, count: 10000}, {dataset: this.price});
            // this.removeList.push(item);
            // console.log(this.removeList);
        }

        private edit() {
            this.isEditing = true;
        }

        private save() {
            _.chunk = function (array, chunkSize) {
                return _.reduce(array, function (reducer, item, index) {
                    reducer.current.push(item);
                    if (reducer.current.length === chunkSize || index + 1 === array.length) {
                        reducer.chunks.push(reducer.current);
                        reducer.current = [];
                    }
                    return reducer;
                }, {current: [], chunks: []}).chunks
            };
            var chunked = _.chunk(this.price, 100);

            var x = 0;
            _.forEach(chunked, item=> {
                this.ImportPriceService.setCityPrice(item, true).then(res=> {
                    x++
                }).catch(err=> {

                });
            });

            if (this.removeList) {
                this.CityPriceService.removeSubtrace(this.removeList).then(res=> {
                    console.log('Удалены: ', res)
                }).catch(err=> {

                })
            }

        }

        private add() {
            this.tableParams.settings().dataset.unshift({
                cityFrom: "",
                cityTo: null,
                field1: null,
                field2: null,
                field3: null,
                field4: null
            });
        }

    }

    angular.module('app').controller('CityPriceController', app.CityPriceController);
}
