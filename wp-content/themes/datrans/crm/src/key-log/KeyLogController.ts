module app {

    declare var angular;
    declare var _;
    declare var jQuery;

    export class KeyLogController {

        static $inject = ['$scope', '$resource', 'myConfig', 'Papa', 'KeyLogService', '$modal', 'NgTableParams'];
        public isLoading:boolean;
        private tableParams;
        private newKeys = {
            key1: '',
            key2: '',
            key3: '',
            key4: '',
        };
        private loading = false;

        constructor(private $scope,
                    private $resource,
                    private myConfig,
                    private Papa,
                    private KeyLogService,
                    private $modal,
                    private NgTableParams) {
            this.$scope.vm = this;
            this.init();
        }

        public init() {
            this.getKeyLog();
        }

        public getKeyLog() {
            this.loading = true;
            this.KeyLogService.getKeyHistory().then(res=> {
                this.loading = false
                if (this.tableParams != 'undefined') {
                    this.tableParams = new this.NgTableParams({page: 1, count: 10000}, {dataset: res});
                } else {
                    this.tableParams.reload();
                }
            }).catch(err=> {
                this.loading = false
            }).finally(()=>{
                this.newKeys = {
                    key1: '',
                    key2: '',
                    key3: '',
                    key4: '',
                };
            })
        }

        public setKey() {
            this.loading = true;
            this.KeyLogService.setKeys(this.newKeys).then(res=> {
                this.loading = false;
                this.getKeyLog();
            }).catch(err=> {
                this.loading = false
            })
        }

        public newKeyIsValid() {
            var isValid = true;
            _.forEach(this.newKeys, item => {
                if (!item.trim()) {
                    isValid = false;
                }
            })
            return isValid;
        }


    }

    angular.module('app').controller('KeyLogController', app.KeyLogController);
}
