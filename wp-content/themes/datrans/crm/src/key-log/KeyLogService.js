var app;
(function (app) {
    var Services;
    (function (Services) {
        var KeyLogService = (function () {
            function KeyLogService($q, $http, myConfig) {
                this.$q = $q;
                this.$http = $http;
                this.myConfig = myConfig;
            }
            KeyLogService.prototype.getKeyHistory = function () {
                var deferred = this.$q.defer();
                this.$http({
                    method: 'GET',
                    url: this.myConfig.ajaxUrl,
                    params: { action: 'get_keys_history' }
                }).success(function (res) {
                    deferred.resolve(res);
                }).error(function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };
            KeyLogService.prototype.setKeys = function (query) {
                var deferred = this.$q.defer();
                this.$http({
                    method: 'POST',
                    url: this.myConfig.ajaxUrl,
                    params: { action: 'set_keys' },
                    data: { keys: query }
                }).success(function (res) {
                    deferred.resolve(res);
                }).error(function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };
            KeyLogService.$inject = ['$q', '$http', 'myConfig'];
            return KeyLogService;
        }());
        Services.KeyLogService = KeyLogService;
    })(Services = app.Services || (app.Services = {}));
    angular.module("app").service('KeyLogService', app.Services.KeyLogService);
})(app || (app = {}));
//# sourceMappingURL=KeyLogService.js.map