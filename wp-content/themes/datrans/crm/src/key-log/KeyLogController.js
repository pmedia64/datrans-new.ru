var app;
(function (app) {
    var KeyLogController = (function () {
        function KeyLogController($scope, $resource, myConfig, Papa, KeyLogService, $modal, NgTableParams) {
            this.$scope = $scope;
            this.$resource = $resource;
            this.myConfig = myConfig;
            this.Papa = Papa;
            this.KeyLogService = KeyLogService;
            this.$modal = $modal;
            this.NgTableParams = NgTableParams;
            this.newKeys = {
                key1: '',
                key2: '',
                key3: '',
                key4: ''
            };
            this.loading = false;
            this.$scope.vm = this;
            this.init();
        }
        KeyLogController.prototype.init = function () {
            this.getKeyLog();
        };
        KeyLogController.prototype.getKeyLog = function () {
            var _this = this;
            this.loading = true;
            this.KeyLogService.getKeyHistory().then(function (res) {
                _this.loading = false;
                if (_this.tableParams != 'undefined') {
                    _this.tableParams = new _this.NgTableParams({ page: 1, count: 10000 }, { dataset: res });
                }
                else {
                    _this.tableParams.reload();
                }
            }).catch(function (err) {
                _this.loading = false;
            }).finally(function () {
                _this.newKeys = {
                    key1: '',
                    key2: '',
                    key3: '',
                    key4: ''
                };
            });
        };
        KeyLogController.prototype.setKey = function () {
            var _this = this;
            this.loading = true;
            this.KeyLogService.setKeys(this.newKeys).then(function (res) {
                _this.loading = false;
                _this.getKeyLog();
            }).catch(function (err) {
                _this.loading = false;
            });
        };
        KeyLogController.prototype.newKeyIsValid = function () {
            var isValid = true;
            _.forEach(this.newKeys, function (item) {
                if (!item.trim()) {
                    isValid = false;
                }
            });
            return isValid;
        };
        KeyLogController.$inject = ['$scope', '$resource', 'myConfig', 'Papa', 'KeyLogService', '$modal', 'NgTableParams'];
        return KeyLogController;
    }());
    app.KeyLogController = KeyLogController;
    angular.module('app').controller('KeyLogController', app.KeyLogController);
})(app || (app = {}));
//# sourceMappingURL=KeyLogController.js.map