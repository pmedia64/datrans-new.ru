<?php

/**
 * Список коэффициентов
 */
function get_keys_history()
{
  $price_db = connect_to_price_db();

  $res = $price_db->get_results(
    "
	SELECT id, key1, key2, key3, key4, dateChange
	FROM " . get_price_table_names()->city_keylog . "
	ORDER BY id DESC
	"
  );

  $encoded = json_encode($res, true);
  echo($encoded);
  wp_die();
}

add_action('wp_ajax_get_keys_history', 'get_keys_history');    // If called from admin panel
add_action('wp_ajax_nopriv_get_keys_history', 'get_keys_history');    // If called from front end


/**
 * Установить коэффициенты
 */
function set_keys()
{
  $price_db = connect_to_price_db();
  $_POST = json_decode(file_get_contents('php://input'), true);
  $request_body = file_get_contents('php://input');
  $data = json_decode($request_body, true)['keys'];
  $response = array();

  $res = $price_db->insert(
    get_price_table_names()->city_keylog,
    array(
      'key1' => trim($data['key1']),
      'key2' => trim($data['key2']),
      'key3' => trim($data['key3']),
      'key4' => trim($data['key4']),
    )
  );
  array_push($response, $res);

  $encoded = json_encode($response);
  echo($encoded);
  wp_die();
}

add_action('wp_ajax_set_keys', 'set_keys');    // If called from admin panel
add_action('wp_ajax_nopriv_set_keys', 'set_keys');    // If called from front end

/**
 * Вернуть актуальный коэффициент
 */
function return_actual_keys()
{
  $price_db = connect_to_price_db();

  $res = $price_db->get_results(
    "
	SELECT key1, key2, key3, key4
	FROM " . get_price_table_names()->city_keylog . "
	ORDER BY id DESC
	LIMIT 1
	"
  );

  return $res[0];
}

add_action('wp_ajax_return_actual_keys', 'return_actual_keys');    // If called from admin panel
add_action('wp_ajax_nopriv_return_actual_keys', 'return_actual_keys');    // If called from front end
