module app {
    declare var angular;
    declare var _;

    export module Services {

        export class KeyLogService {
            static $inject = ['$q', '$http', 'myConfig'];

            constructor(private $q,
                        private $http,
                        private myConfig) {
            }

            public getKeyHistory() {
                var deferred = this.$q.defer();

                this.$http({
                    method: 'GET',
                    url: this.myConfig.ajaxUrl,
                    params: {action: 'get_keys_history'}
                }).success(function (res:any) {
                    deferred.resolve(res);
                }).error((error)=> {
                    deferred.reject(error);
                });
                return deferred.promise;
            }

            public setKeys(query) {
                var deferred = this.$q.defer();
                this.$http({
                    method: 'POST',
                    url: this.myConfig.ajaxUrl,
                    params: {action: 'set_keys'},
                    data: {keys: query}
                }).success(function (res:any) {
                    deferred.resolve(res);
                }).error((error)=> {
                    deferred.reject(error);
                });
                return deferred.promise;
            }


        }
    }
    angular.module("app").service('KeyLogService', app.Services.KeyLogService);
}