<?php

/*
* Список городов GET
*/
function get_city_list()
{
    $response = array();
  $price_db = connect_to_price_db();
    $res = $price_db->get_results(
        "
	SELECT id, name, defaultCity
	FROM ".get_price_table_names()->city_list."
	");
    
    if (count($res) > 0) {
        array_push($response, $res);
    } else {
//        http_response_code(404);
//        wp_die(json_encode(array('message' => 'ERROR', 'code' => 500)));
    }

    $encoded = json_encode($response[0], 256);
    echo($encoded);
    wp_die();
}

add_action('wp_ajax_get_city_list', 'get_city_list');    // If called from admin panel
add_action('wp_ajax_nopriv_get_city_list', 'get_city_list');    // If called from front end


/**
 * Удалить направление
 */
function remove_trace()
{
  $price_db = connect_to_price_db();
    $_POST = json_decode(file_get_contents('php://input'), true);
    $request_body = file_get_contents('php://input');

    $data = json_decode($request_body, true);

    $response = array();

        $cityFrom = $data['name'];

        $res1 = $price_db->query(
          $price_db->prepare(
                "DELETE FROM " . get_price_table_names()->city_price . "
		 WHERE cityFrom = %s",
                $cityFrom
            )
        );

        $res2 = $price_db->query(
          $price_db->prepare(
                "DELETE FROM " . get_price_table_names()->city_list . "
		 WHERE name = %s",
                $cityFrom
            )
        );

        $item = array(
            'fromPrice' => $res1,
            'fromList' => $res2,
            'cityFrom' => $cityFrom
        );

        array_push($response, $item);


    $encoded = json_encode($response, TRUE);
    print_r($encoded);
    wp_die();


}

add_action('wp_ajax_remove_trace', 'remove_trace');    // If called from admin panel
add_action('wp_ajax_nopriv_remove_trace', 'remove_trace');    // If called from front end


/**
 * Установить город по умолчанию
 */
function set_default_city()
{
  $price_db = connect_to_price_db();
  $_POST = json_decode(file_get_contents('php://input'), true);
  $request_body = file_get_contents('php://input');

  $data = json_decode($request_body, true);

  $response = $price_db->update(
    get_price_table_names()->city_list,
    array(
      'defaultCity' => (int)trim($data['value']),
    ),
    array(
      'id' => trim($data['id']),
    )
  );

  $encoded = json_encode($response, TRUE);
  print_r($encoded);
  wp_die();


}

add_action('wp_ajax_set_default_city', 'set_default_city');    // If called from admin panel
add_action('wp_ajax_nopriv_set_default_city', 'set_default_city');    // If called from front end
