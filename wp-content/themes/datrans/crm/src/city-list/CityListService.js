var app;
(function (app) {
    var Services;
    (function (Services) {
        var CityListService = (function () {
            function CityListService($q, $http, myConfig) {
                this.$q = $q;
                this.$http = $http;
                this.myConfig = myConfig;
            }
            CityListService.prototype.getCityList = function () {
                var deferred = this.$q.defer();
                this.$http({
                    method: 'GET',
                    url: this.myConfig.ajaxUrl,
                    params: { action: 'get_city_list' }
                }).success(function (res) {
                    deferred.resolve(res);
                }).error(function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };
            CityListService.prototype.removeTrace = function (query) {
                var deferred = this.$q.defer();
                this.$http({
                    method: 'POST',
                    url: this.myConfig.ajaxUrl,
                    params: { action: 'remove_trace' },
                    data: query,
                }).success(function (res) {
                    deferred.resolve(res);
                }).error(function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };
            CityListService.prototype.setDefault = function (id, value) {
                var deferred = this.$q.defer();
                this.$http({
                    method: 'POST',
                    url: this.myConfig.ajaxUrl,
                    params: { action: 'set_default_city' },
                    data: { id: id, value: value },
                }).success(function (res) {
                    deferred.resolve(res);
                }).error(function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };
            CityListService.$inject = ['$q', '$http', 'myConfig'];
            return CityListService;
        }());
        Services.CityListService = CityListService;
    })(Services = app.Services || (app.Services = {}));
    angular.module("app").service('CityListService', app.Services.CityListService);
})(app || (app = {}));
