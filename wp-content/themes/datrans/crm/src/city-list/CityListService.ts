module app {
    declare var angular;
    declare var _;

    export module Services {

        export class CityListService {
            static $inject = ['$q', '$http', 'myConfig'];

            constructor(private $q,
                        private $http,
                        private myConfig) {
            }

            public getCityList() {
                var deferred = this.$q.defer();
                this.$http({
                    method: 'GET',
                    url: this.myConfig.ajaxUrl,
                    params: {action: 'get_city_list'}
                }).success(function (res:any) {
                    deferred.resolve(res);
                }).error((error)=> {
                    deferred.reject(error);
                });
                return deferred.promise;
            }

            public removeTrace(query) {
                var deferred = this.$q.defer();
                this.$http({
                    method: 'POST',
                    url: this.myConfig.ajaxUrl,
                    params: {action: 'remove_trace'},
                    data: query,
                }).success(function (res:any) {
                    deferred.resolve(res);
                }).error((error)=> {
                    deferred.reject(error);
                });
                return deferred.promise;
            }

            public setDefault(id, value) {
                var deferred = this.$q.defer();
                this.$http({
                    method: 'POST',
                    url: this.myConfig.ajaxUrl,
                    params: {action: 'set_default_city'},
                    data: {id, value},
                }).success(function (res:any) {
                    deferred.resolve(res);
                }).error((error)=> {
                    deferred.reject(error);
                });
                return deferred.promise;
            }

        }
    }
    angular.module("app").service('CityListService', app.Services.CityListService);
}