var app;
(function (app) {
    var CityListController = (function () {
        function CityListController($scope, NgTableParams, $resource, myConfig, CityListService, ImportPriceService, $rootScope, $state, $stateParams) {
            this.$scope = $scope;
            this.NgTableParams = NgTableParams;
            this.$resource = $resource;
            this.myConfig = myConfig;
            this.CityListService = CityListService;
            this.ImportPriceService = ImportPriceService;
            this.$rootScope = $rootScope;
            this.$state = $state;
            this.$stateParams = $stateParams;
            this.loading = {
                load: false,
                remove: false
            };
            this.checkboxes = {
                'checked': false,
                items: {}
            };
            this.$scope.vm = this;
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
            this.init();
        }
        CityListController.prototype.init = function () {
            this.getCityList();
        };
        CityListController.prototype.getCityList = function () {
            var _this = this;
            this.loading.load = true;
            this.CityListService.getCityList().then(function (res) {
                _this.loading.load = false;
                _this.cityList = res;
                if (_this.tableParams != 'undefined') {
                    _this.tableParams = new _this.NgTableParams({ page: 1, count: 10000 }, { dataset: _this.cityList });
                }
                else {
                    _this.tableParams.settings.reload();
                    _this.tableParams.reload();
                }
                console.log('getCityList', res);
            }).catch(function (err) {
            }).finally(function () {
                _this.loading.load = false;
            });
        };
        CityListController.prototype.goToPrice = function (city) {
            this.$rootScope.$state.go('city-price', { id: city.id });
        };
        CityListController.prototype.setDefault = function (id, value) {
            var _this = this;
            this.CityListService.setDefault(id, value).then(function (res) {
                _this.cityList[id - 1].defaultCity = value;
            });
        };
        CityListController.prototype.delPrice = function () {
            var isYes = confirm("Будут удалены прайсы выбранных городов");
            if (isYes) {
                this.checkboxes.items;
            }
        };
        CityListController.prototype.deleteTrace = function () {
            var _this = this;
            var isYes = confirm("Будут удалены прайсы выбранных городов");
            var removePriceArr = [];
            if (isYes) {
                this.loading.remove = true;
                _.forEach(this.cityList, function (item) {
                    _.forEach(_this.checkboxes.items, function (checkbox, key) {
                        if (item.id == key && checkbox == true)
                            removePriceArr.push(item);
                    });
                });
                var i = 0;
                _.forEach(removePriceArr, function (item) {
                    _this.CityListService.removeTrace(item).then(function (res) {
                        i++;
                        if (removePriceArr.length == i) {
                            _this.getCityList();
                            _this.checkboxes.items = {};
                            _this.loading.remove = false;
                        }
                    }).catch(function (err) {
                        _this.loading.remove = false;
                    });
                });
            }
            console.log(removePriceArr);
        };
        CityListController.prototype.truncate = function () {
            var _this = this;
            var isTruncate = confirm("Вы хотите очистить ВСЕ прайсы в БД.");
            if (isTruncate) {
                this.loading.remove = true;
                this.ImportPriceService.truncatePrice().then(function (res) {
                    _this.loading.remove = false;
                }).catch(function (err) {
                    _this.loading.remove = false;
                });
            }
        };
        CityListController.$inject = ['$scope', 'NgTableParams', '$resource', 'myConfig', 'CityListService', 'ImportPriceService', '$rootScope', '$state', '$stateParams'];
        return CityListController;
    }());
    app.CityListController = CityListController;
    angular.module('app').controller('CityListController', app.CityListController);
})(app || (app = {}));
/**
 * sideNavigation - Directive for run metsiMenu on sidebar navigation
 */
angular.module('app').directive('metisMenu', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element) {
                // Call the metsiMenu plugin and plug it to sidebar navigation
                $timeout(function () {
                    element.metisMenu();
                });
            }
        };
    }]);
