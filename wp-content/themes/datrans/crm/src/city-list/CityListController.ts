module app {

    declare var angular;
    declare var _;
    declare var jQuery;

    export class CityListController {

        static $inject = ['$scope', 'NgTableParams', '$resource', 'myConfig', 'CityListService', 'ImportPriceService', '$rootScope', '$state', '$stateParams'];
        public isLoading:boolean;
        private tableParams;
        public cityList;
        public loading =
        {
            load: false,
            remove: false
        };
        public checkboxes={
            'checked': false,
            items: {}
        };

        constructor(private $scope,
                    private NgTableParams,
                    private $resource,
                    private myConfig,
                    private CityListService,
                    private ImportPriceService,
                    private $rootScope,
                    private $state,
                    private $stateParams) {
            this.$scope.vm = this;
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
            this.init();
        }

        public init() {
            this.getCityList();
        }

        private getCityList() {
            this.loading.load = true;
            this.CityListService.getCityList().then(res=> {
                this.loading.load = false;
                this.cityList = res;
                if (this.tableParams != 'undefined') {
                    this.tableParams = new this.NgTableParams({page: 1, count: 10000}, {dataset: this.cityList});
                } else {
                    this.tableParams.settings.reload();
                    this.tableParams.reload();
                }
                console.log('getCityList', res);
            }).catch(err=> {

            }).finally(()=> {
                this.loading.load = false;
            })
        }

        private goToPrice(city) {
            this.$rootScope.$state.go('city-price', {id: city.id});
        }

        private setDefault(id:number, value) {
            this.CityListService.setDefault(id, value).then(res =>{
                this.cityList[id-1].defaultCity = value
            });
        }

        private delPrice() {
            var isYes = confirm("Будут удалены прайсы выбранных городов");

            if (isYes) {
                this.checkboxes.items;
            }
        }

        private deleteTrace() {
            var isYes = confirm("Будут удалены прайсы выбранных городов");
            var removePriceArr = [];
            if (isYes) {
                this.loading.remove = true;
                _.forEach(this.cityList, item=> {
                    _.forEach(this.checkboxes.items, (checkbox, key)=> {
                        if (item.id == key && checkbox == true)
                            removePriceArr.push(item)
                    });
                });

                var i = 0;
                _.forEach(removePriceArr, item=> {
                    this.CityListService.removeTrace(item).then(res=> {
                        i++;
                        if (removePriceArr.length == i) {
                            this.getCityList();
                            this.checkboxes.items = {};
                            this.loading.remove = false;
                        }
                    }).catch(err=>{
                        this.loading.remove = false;
                    });
                });
            }
            console.log(removePriceArr);
        }

        public truncate() {
            var isTruncate = confirm("Вы хотите очистить ВСЕ прайсы в БД.");
            if (isTruncate) {
                this.loading.remove = true;
                this.ImportPriceService.truncatePrice().then(res=> {
                    this.loading.remove = false;
                }).catch(err=>{
                    this.loading.remove = false;
                })
            }
        }

    }

    angular.module('app').controller('CityListController', app.CityListController);
}

declare var angular;

/**
 * sideNavigation - Directive for run metsiMenu on sidebar navigation
 */
angular.module('app').directive('metisMenu', ['$timeout', ($timeout) => {
    return {
        restrict: 'A',
        link: function (scope, element) {
            // Call the metsiMenu plugin and plug it to sidebar navigation
            $timeout(function () {
                element.metisMenu();

            });
        }
    };
}]);