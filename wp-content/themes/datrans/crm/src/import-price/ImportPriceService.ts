module app {
    declare var angular;
    declare var _;

    export module Services {

        export class ImportPriceService {
            static $inject = ['$q', '$http', 'myConfig'];

            constructor(private $q,
                        private $http,
                        private myConfig) {
            }

            public setCityList(items) {
                var deferred = this.$q.defer();

                this.$http({
                    method: 'POST',
                    url: this.myConfig.ajaxUrl,
                    params: {action: 'set_city_list'},
                    headers: {'Content-Type': 'application/json'},
                    data: JSON.stringify(items),
                    transformRequest: null
                }).success(function (res:any) {
                    deferred.resolve(res);
                }).error((error)=> {
                    deferred.reject(error);
                });
                return deferred.promise;
            }

            public setCityPrice(items, isUpdate) {
                var deferred = this.$q.defer();

                this.$http({
                    method: 'POST',
                    url: this.myConfig.ajaxUrl,
                    params: {action: 'set_price_city', isUpdate: isUpdate},
                    headers: {'Content-Type': 'application/json'},
                    data: JSON.stringify(items),
                    transformRequest: null
                }).success(function (res:any) {
                    deferred.resolve(res);
                }).error((error)=> {
                    deferred.reject(error);
                });
                return deferred.promise;
            }
            
            public truncatePrice() {
                var deferred = this.$q.defer();

                this.$http({
                    method: 'POST',
                    url: this.myConfig.ajaxUrl,
                    params: {action: 'truncatePrice'},
                    headers: {'Content-Type': 'application/json'},
                    transformRequest: null
                }).success(function (res:any) {
                    deferred.resolve(res);
                }).error((error)=> {
                    deferred.reject(error);
                });
                return deferred.promise;
            }
            
            public importCSV(fd?) {
                var deferred = this.$q.defer();

                this.$http({
                    method: 'POST',
                    url: this.myConfig.ajaxUrl,
                    params: {action: 'import_csv'},
                    data: fd,
                    processData: false,
                     headers : { 'Content-Type': undefined}
                }).success(function (res:any) {
                    deferred.resolve(res);
                }).error((error)=> {
                    deferred.reject(error);
                });
                return deferred.promise;
            }

        }
    }
    angular.module("app").service('ImportPriceService', app.Services.ImportPriceService);
}