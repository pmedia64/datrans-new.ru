var app;
(function (app) {
    var Services;
    (function (Services) {
        var ImportPriceService = (function () {
            function ImportPriceService($q, $http, myConfig) {
                this.$q = $q;
                this.$http = $http;
                this.myConfig = myConfig;
            }
            ImportPriceService.prototype.setCityList = function (items) {
                var deferred = this.$q.defer();
                this.$http({
                    method: 'POST',
                    url: this.myConfig.ajaxUrl,
                    params: { action: 'set_city_list' },
                    headers: { 'Content-Type': 'application/json' },
                    data: JSON.stringify(items),
                    transformRequest: null
                }).success(function (res) {
                    deferred.resolve(res);
                }).error(function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };
            ImportPriceService.prototype.setCityPrice = function (items, isUpdate) {
                var deferred = this.$q.defer();
                this.$http({
                    method: 'POST',
                    url: this.myConfig.ajaxUrl,
                    params: { action: 'set_price_city', isUpdate: isUpdate },
                    headers: { 'Content-Type': 'application/json' },
                    data: JSON.stringify(items),
                    transformRequest: null
                }).success(function (res) {
                    deferred.resolve(res);
                }).error(function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };
            ImportPriceService.prototype.truncatePrice = function () {
                var deferred = this.$q.defer();
                this.$http({
                    method: 'POST',
                    url: this.myConfig.ajaxUrl,
                    params: { action: 'truncatePrice' },
                    headers: { 'Content-Type': 'application/json' },
                    transformRequest: null
                }).success(function (res) {
                    deferred.resolve(res);
                }).error(function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };
            ImportPriceService.prototype.importCSV = function (fd) {
                var deferred = this.$q.defer();
                this.$http({
                    method: 'POST',
                    url: this.myConfig.ajaxUrl,
                    params: { action: 'import_csv' },
                    data: fd,
                    processData: false,
                    headers: { 'Content-Type': undefined }
                }).success(function (res) {
                    deferred.resolve(res);
                }).error(function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };
            ImportPriceService.$inject = ['$q', '$http', 'myConfig'];
            return ImportPriceService;
        }());
        Services.ImportPriceService = ImportPriceService;
    })(Services = app.Services || (app.Services = {}));
    angular.module("app").service('ImportPriceService', app.Services.ImportPriceService);
})(app || (app = {}));
//# sourceMappingURL=ImportPriceService.js.map