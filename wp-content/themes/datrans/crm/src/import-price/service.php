<?php

/**
 * Список городов POST
 */
function set_city_list()
{
  $price_db = connect_to_price_db();

    $_POST = json_decode(file_get_contents('php://input'), true);
    $request_body = file_get_contents('php://input');

    $data = json_decode($request_body, true);
    $response = array();

    foreach ($data as $city) {
        if ($city) {
            //array_push($response, $city);

            $res = $price_db->get_results(
                "
	SELECT id, name
	FROM ".get_price_table_names()->city_list."
	WHERE name = '$city'
	"
            );
            //если город уже есть => вернуть его
            if (count($res) > 0) {
                array_push($response, $res);
            } else { //если города нет => записать
                $item = setItem($city);
                array_push($response, $item);
            }
        }
    }


    $encoded = json_encode($response);
    echo($encoded);
    wp_die();


}

/**
 * Insert city
 * @param $item
 * @return array
 */
function setItem($item)
{
  $price_db = connect_to_price_db();
    $res = $price_db->insert(
        get_price_table_names()->city_list,
        array(
            'name' => $item,
        )
    );

    $res = array(
        'id' => $price_db->insert_id,
        'name' => $item
    );

    return $res;
}


add_action('wp_ajax_set_city_list', 'set_city_list');    // If called from admin panel
add_action('wp_ajax_nopriv_set_city_list', 'set_city_list');    // If called from front end


/**
 * Записать прайс POST
 */
function set_price_city()
{
  $price_db = connect_to_price_db();
    $isUpdate = trim($_GET['isUpdate']);
    $_POST = json_decode(file_get_contents('php://input'), true);
    $request_body = file_get_contents('php://input');

    $data = json_decode($request_body, true);
    $response = array();
    $i = 0;
    foreach ($data as $price) {
        $i++;
        if ($price) {
            $cityFrom = $price['cityFrom'];
            $cityTo = $price['cityTo'];

            $result = $price_db->get_results(
                "SELECT id
	FROM ".get_price_table_names()->city_price."
	WHERE cityFrom = '$cityFrom' AND cityTo = '$cityTo'	"
            );

            //если город уже есть => вернуть его и обновить
            if (count($result[0]) > 0 && $isUpdate == 'true') {
                $item = null;
                $item = updateItemPrice($price);
                $res = array(
                    'ids' => $item,
                    'cityFrom' => $cityFrom,
                    'cityTo' => $cityTo,
                    'field1' => $price['field1'],
                    'field2' => $price['field2'],
                    'field3' => $price['field3'],
                    'field4' => $price['field4'],
                    'update' => true
                );
                array_push($response, $res);
            }

            if (count($result[0]) == 0 && $isUpdate == 'true') {
                $item = setItemPrice($price);
                array_push($response, $item);
            }
            if ($isUpdate == 'false') {
                $item = setItemPrice($price);
                array_push($response, $item);
            }

        }
    }

    $encoded = json_encode($response, TRUE);
    print_r($encoded);
    wp_die();
}

/**
 * Insert price
 * @param $item
 * @return array
 */
function setItemPrice($item)
{
  $price_db = connect_to_price_db();
    $res = $price_db->insert(
        get_price_table_names()->city_price,
        array(
            'cityFrom' => trim($item['cityFrom']),
            'cityTo' => trim($item['cityTo']),
            'field1' => (int)str_replace(' ', '', $item['field1']),
            'field2' => (int)str_replace(' ', '', $item['field2']),
            'field3' => (int)str_replace(' ', '', $item['field3']),
            'field4' => (int)str_replace(' ', '', $item['field4']),
        )
    );

    $res = array(
        'id' => $price_db->insert_id,
        'cityFrom' => trim($item['cityFrom']),
        'cityTo' => trim($item['cityTo']),
        'update' => false
    );

    return $res;
}

/**
 * Insert price
 * @param $item
 * @return array
 */
function updateItemPrice($item)
{
  $price_db = connect_to_price_db();

    $res = $price_db->update(
        get_price_table_names()->city_price,
        array(
            'field1' => (int)str_replace(' ', '', $item['field1']),
            'field2' => (int)str_replace(' ', '', $item['field2']),
            'field3' => (int)str_replace(' ', '', $item['field3']),
            'field4' => (int)str_replace(' ', '', $item['field4'])
        ),
        array(
            'cityFrom' => trim($item['cityFrom']),
            'cityTo' => trim($item['cityTo']),
        )
    );

    return $res;
}

add_action('wp_ajax_set_price_city', 'set_price_city');    // If called from admin panel
add_action('wp_ajax_nopriv_set_price_city', 'set_price_city');    // If called from front end


function truncatePrice()
{
  $price_db = connect_to_price_db();
    $delete_price = $price_db->query("TRUNCATE TABLE `dtrs_city_price`");
    $delete_list = $price_db->query("TRUNCATE TABLE `dtrs_city_list`");
    echo($delete_price);
    wp_die();
}

add_action('wp_ajax_truncatePrice', 'truncatePrice');    // If called from admin panel
add_action('wp_ajax_nopriv_truncatePrice', 'truncatePrice');    // If called from front end


/**
 * Insert how csv
 * @param $item
 * @return array
 */
function import_csv()
{
  $price_db = connect_to_price_db();

    $response = array();
    $filename = $_FILES['file']['tmp_name'];
    $filename = str_replace("\\","//",$filename);

    $lineend = '\\r\\n';    // Чем заканчивается строка в файле CSV
    $afields = array("cityFrom", "cityTo", "field1", "field2", "field3", "field4");

    $table_nme = get_price_table_names()->city_price;
    $sql = "LOAD DATA LOCAL INFILE '" . $filename . "'
	INTO TABLE $table_nme
	FIELDS TERMINATED BY ';'
	ENCLOSED BY '\"'
	ESCAPED BY '\"'
	LINES TERMINATED BY '" . $lineend . "'
	IGNORE 1 LINES
	(".implode(',', $afields).")
	";
    $res = $price_db->query($sql);

    array_push($response, $filename);
    array_push($response, $res[0]);

    $encoded = json_encode($response, TRUE);
    print_r($encoded);
    wp_die();
}

add_action('wp_ajax_import_csv', 'import_csv');    // If called from admin panel
add_action('wp_ajax_nopriv_import_csv', 'import_csv');    // If called from front end
