/// <reference path="modal/showItem.ts" />
var app;
(function (app) {
    var ImportPriceController = (function () {
        function ImportPriceController($scope, $resource, myConfig, Papa, ImportPriceService, $modal, $window) {
            this.$scope = $scope;
            this.$resource = $resource;
            this.myConfig = myConfig;
            this.Papa = Papa;
            this.ImportPriceService = ImportPriceService;
            this.$modal = $modal;
            this.$window = $window;
            this.cityList = [];
            this.loading = false;
            this.message = '';
            this.isCSVImport = false;
            this.i = 0;
            this.$scope.vm = this;
            this.init();
        }
        ImportPriceController.prototype.init = function () {
        };
        ImportPriceController.prototype.importCSV = function (csv) {
            var _this = this;
            this.setItems(this.cityList);
            this.loading = true;
            //  var
            //      form = jQuery('form');
            // var formData = new FormData(form);
            var fd = new FormData(jQuery('#csv').get(0).files[0]);
            fd.append('file', jQuery('#csv').get(0).files[0]);
            this.ImportPriceService.importCSV(fd).then(function (res) {
                _this.message = 'Импорт CSV: УСПЕШНО ' + '<br>' + _this.message;
                _this.message = 'Проверьте прайсы на наличие ошибок ' + '<br>' + _this.message;
                _this.loading = false;
            }).catch(function (err) {
                _this.loading = false;
            });
        };
        ImportPriceController.prototype.readFile = function (csv, isImportCSV) {
            var _this = this;
            this.items = [];
            this.cityList = [];
            if (isImportCSV) {
                this.isCSVImport = true;
            }
            else {
                this.isCSVImport = false;
            }
            this.loading = true;
            this.message = 'Чтение файла..';
            var ccc = jQuery('#csv').get(0).files[0];
            this.Papa.parse(ccc, {
                header: true,
                delimiter: ';',
                dynamicTyping: true,
                encoding: "UTF-8"
            })
                .then(function (res) {
                _this.items = _.groupBy(res.data, 'cityFrom');
                var cityListDerty = _.map(_this.items, function (num, key) {
                    if (key) {
                        return key;
                    }
                });
                _.forEach(cityListDerty, function (key) {
                    if (key) {
                        _this.cityList.push(key);
                    }
                });
                _this.loading = false;
                _this.message = 'Чтение файла: УСПЕШНО ' + '<br>' + _this.message;
            })
                .catch(function (err) {
                _this.message += '<br> Ошибка чтения...' + err;
                _this.loading = false;
            })
                .finally();
        };
        ImportPriceController.prototype.update = function () {
            this.loading = true;
            this.setItems(this.cityList);
            this.setPrices(this.items, true);
        };
        ImportPriceController.prototype.insert = function () {
            this.setItems(this.cityList);
            this.setPrices(this.items, false);
        };
        ImportPriceController.prototype.setItems = function (items) {
            var _this = this;
            this.loading = true;
            this.message = ' Запись списка городов..<br>' + this.message;
            this.ImportPriceService.setCityList(items).then(function (res) {
                _this.message = 'Запись списка городов. <span class="success">УСПЕШНО</span><br>' + _this.message;
                console.log('Запись списка городов..', res);
            }).catch(function (err) {
            });
        };
        //Синхронный импорт
        ImportPriceController.prototype.setPrices = function (items, isUpdate) {
            var _this = this;
            if (this.cityList.length <= this.i) {
                this.message = 'Готово ' + '<br>' + this.message;
                this.loading = false;
                return false;
            }
            var thisKey = '';
            var thisCity = _.find(items, function (item, key) {
                thisKey = key;
                return _this.cityList[_this.i] == key;
            });
            var uniqThisCity = _.uniq(thisCity, function (item, key, cityTo) {
                return [item.cityTo, item.cityFrom].join();
            });
            this.message = 'Удаление дублей в городах: УСПЕШНО ' + '<br>' + this.message;
            //TODO делю массив на части по 100
            _.chunk = function (array, chunkSize) {
                return _.reduce(array, function (reducer, item, index) {
                    reducer.current.push(item);
                    if (reducer.current.length === chunkSize || index + 1 === array.length) {
                        reducer.chunks.push(reducer.current);
                        reducer.current = [];
                    }
                    return reducer;
                }, { current: [], chunks: [] }).chunks;
            };
            var chunked = _.chunk(uniqThisCity, 100);
            this.message = 'Запись прайса: ' + thisKey + '<br>' + this.message;
            var x = 0;
            _.forEach(chunked, function (item) {
                _this.ImportPriceService.setCityPrice(item, isUpdate).then(function (res) {
                    if (!res) {
                        _this.message = 'Запись прайса: ОШИБКА - ' + item.name + '<br>' + _this.message;
                    }
                    x++;
                    if (x >= chunked.length) {
                        _this.i++;
                        _this.setPrices(items, isUpdate);
                    }
                }).catch(function (err) {
                    _this.message = 'Запись прайса: ОШИБКА' + thisKey + '<br>' + _this.message;
                    _this.i++;
                    _this.setPrices(items);
                });
            });
        };
        ImportPriceController.prototype.showItem = function (item) {
            var showItem = _.find(this.items, function (n, key) {
                return item == key;
            });
            var modalInstance = this.$modal.open({
                templateUrl: this.myConfig.themeUrl + 'crm/src/import-price/modal/view.html',
                size: 'lg',
                controller: app.ImportShowItemController,
                resolve: {
                    item: function () { return showItem; }
                }
            });
        };
        ImportPriceController.prototype.remove = function (item) {
            this.cityList = _.without(this.cityList, item);
            console.log('this.cityList', this.cityList);
        };
        ImportPriceController.$inject = ['$scope', '$resource', 'myConfig', 'Papa', 'ImportPriceService', '$modal'];
        return ImportPriceController;
    }());
    app.ImportPriceController = ImportPriceController;
    angular.module('app').controller('ImportPriceController', app.ImportPriceController);
})(app || (app = {}));
//# sourceMappingURL=ImportPriceController.js.map