module app {

    declare var angular;
    declare var _;
    declare var jQuery;

    export class ImportShowItemController {

        static $inject = ['$scope', '$resource', 'myConfig', '$modal', 'item'];
        public isLoading:boolean;
        private tableParams;
        private items;
        private cityList = [];
        private loading = false;
        private message = '';

        constructor(private $scope,
                    private $resource,
                    private myConfig,
                    private $modal,
                    private item,
                    private $window) {
            this.$scope.vm = this;
            this.init();
            this.items = item;
        }

        public init() {

        }

    }

    angular.module('app').controller('ImportShowItemController', app.ImportShowItemController);
}
