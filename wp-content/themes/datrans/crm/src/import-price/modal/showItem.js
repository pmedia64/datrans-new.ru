var app;
(function (app) {
    var ImportShowItemController = (function () {
        function ImportShowItemController($scope, $resource, myConfig, $modal, item, $window) {
            this.$scope = $scope;
            this.$resource = $resource;
            this.myConfig = myConfig;
            this.$modal = $modal;
            this.item = item;
            this.$window = $window;
            this.cityList = [];
            this.loading = false;
            this.message = '';
            this.$scope.vm = this;
            this.init();
            this.items = item;
        }
        ImportShowItemController.prototype.init = function () {
        };
        ImportShowItemController.$inject = ['$scope', '$resource', 'myConfig', '$modal', 'item'];
        return ImportShowItemController;
    }());
    app.ImportShowItemController = ImportShowItemController;
    angular.module('app').controller('ImportShowItemController', app.ImportShowItemController);
})(app || (app = {}));
//# sourceMappingURL=showItem.js.map