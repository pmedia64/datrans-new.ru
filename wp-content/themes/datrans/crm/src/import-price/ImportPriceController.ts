/// <reference path="modal/showItem.ts" />

module app {

    declare var angular;
    declare var _;
    declare var jQuery;

    export class ImportPriceController {

        static $inject = ['$scope', '$resource', 'myConfig', 'Papa', 'ImportPriceService', '$modal'];
        public isLoading:boolean;
        private tableParams;
        private items;
        private csv;
        private cityList = [];
        private loading = false;
        private message = '';
        private isCSVImport = false;

        constructor(private $scope,
                    private $resource,
                    private myConfig,
                    private Papa,
                    private ImportPriceService,
                    private $modal,
                    private $window) {
            this.$scope.vm = this;
            this.init();

        }

        public init() {

        }

        public importCSV(csv) {
            this.setItems(this.cityList);
            this.loading = true;
            //  var
            //      form = jQuery('form');
            // var formData = new FormData(form);

            var fd = new FormData(jQuery('#csv').get(0).files[0]);
            fd.append('file', jQuery('#csv').get(0).files[0]);
            this.ImportPriceService.importCSV(fd).then(res=> {
                this.message = 'Импорт CSV: УСПЕШНО ' + '<br>' + this.message;
                this.message = 'Проверьте прайсы на наличие ошибок ' + '<br>' + this.message;
                this.loading = false;
            }).catch(err=> {
                this.loading = false;
            });
        }

        public readFile(csv, isImportCSV?){
            this.items = [];
            this.cityList = [];
            if(isImportCSV){
                this.isCSVImport = true;
            }else{
                this.isCSVImport = false;
            }
            this.loading = true;
            this.message = 'Чтение файла..';
            var ccc = jQuery('#csv').get(0).files[0];
            this.Papa.parse(ccc, {
                header: true,
                delimiter: ';',
                dynamicTyping: true,
                encoding: "UTF-8",
            })
                .then((res)=> {
                    this.items = _.groupBy(res.data, 'cityFrom');
                    var cityListDerty = _.map(this.items,   (num, key) =>{
                        if (key) {
                            return key;
                        }
                    });
                    _.forEach(cityListDerty,   (key) => {
                        if (key) {
                            this.cityList.push(key);
                        }
                    });
                    this.loading = false;
                    this.message = 'Чтение файла: УСПЕШНО ' + '<br>' + this.message;
                })
                .catch(err=> {
                    this.message += '<br> Ошибка чтения...' + err;
                    this.loading = false;
                })
                .finally(

                );
        }

        public update() {
            this.loading = true;
            this.setItems(this.cityList);
            this.setPrices(this.items, true);
        }

        public insert() {
            this.setItems(this.cityList);
            this.setPrices(this.items, false);
        }
        
        private setItems(items) {
            this.loading = true;
            this.message = ' Запись списка городов..<br>' + this.message;
            this.ImportPriceService.setCityList(items).then(res=> {
                this.message = 'Запись списка городов. <span class="success">УСПЕШНО</span><br>' + this.message;
                console.log('Запись списка городов..', res);
            }).catch(err=> {

            });
        }

        private i = 0;
//Синхронный импорт
        private setPrices(items, isUpdate?:boolean) {
            if (this.cityList.length <= this.i) {
                this.message = 'Готово ' + '<br>' + this.message;
                this.loading = false;
                return false
            }
            var thisKey = '';
            var thisCity = _.find(items, (item, key)=> {
                thisKey = key;
                return this.cityList[this.i] == key
            });
            var uniqThisCity = _.uniq(thisCity, function(item, key, cityTo) {
                return [item.cityTo, item.cityFrom].join();
            });
            this.message = 'Удаление дублей в городах: УСПЕШНО ' + '<br>' + this.message;
//TODO делю массив на части по 100
            _.chunk = function (array, chunkSize) {
                return _.reduce(array, function (reducer, item, index) {
                    reducer.current.push(item);
                    if (reducer.current.length === chunkSize || index + 1 === array.length) {
                        reducer.chunks.push(reducer.current);
                        reducer.current = [];
                    }
                    return reducer;
                }, {current: [], chunks: []}).chunks
            };

            var chunked = _.chunk(uniqThisCity, 100);
            this.message = 'Запись прайса: ' + thisKey + '<br>' + this.message;

            var x = 0;
            _.forEach(chunked, item=> {

                this.ImportPriceService.setCityPrice(item, isUpdate).then(res=> {
                    if(!res){
                        this.message = 'Запись прайса: ОШИБКА - ' + item.name + '<br>' + this.message;
                    }
                    x++;
                    if (x >= chunked.length) {
                        this.i++;
                        this.setPrices(items, isUpdate);
                    }
                }).catch(err=> {
                    this.message = 'Запись прайса: ОШИБКА' + thisKey + '<br>' + this.message;
                    this.i++;
                    this.setPrices(items);
                });
            });


        }


        private showItem(item) {
            var showItem = _.find(this.items, (n, key)=> {
                return item == key
            });
            var modalInstance = this.$modal.open({
                templateUrl: this.myConfig.themeUrl + 'crm/src/import-price/modal/view.html',
                size: 'lg',
                controller: ImportShowItemController,
                resolve: {
                    item: () => showItem
                }
            });
        }

        private remove(item) {
            this.cityList = _.without(this.cityList, item);
            console.log('this.cityList', this.cityList);
        }
    }

    angular.module('app').controller('ImportPriceController', app.ImportPriceController);
}
