<?php
/**
 * Template Name: Template CRM
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <?php wp_head(); ?>
    <link rel='stylesheet' href="<?php echo esc_url(get_template_directory_uri()); ?>/crm/lib/bootstrap/bootstrap.css"/>
    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/crm/lib/bootstrap/bootstrap.js"></script>
</head>

<body>
<div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation" metisMenu>
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">

                <div class="tabs-container pace-done">
                    <div class="tabs-left " id="side-menu">
                        <ul class="nav metismenu nav-tabs-sticky">
                            <li>
                                <a href="#/city-list"><i class="fa fa-wallet"></i>
                                    <span class="nav-label">Прайсы</span></a>
                            </li>
                            <li>
                                <a href="#/key-log"><i class="fa fa-wallet"></i>
                                    <span class="nav-label">Коэффициент</span></a>
                            </li>
                            <li>
                                <a href="#/import-price"><i class="fa fa-wallet"></i>
                                    <span class="nav-label">Импорт прайса</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </ul>
        </div>
    </nav>
    <div id="page-wrapper">
        <div ng-app="app">

            <div class="ibox col-sm-12"></div>
            <div ui-view></div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>

<link href="<?php echo esc_url(get_template_directory_uri()); ?>/crm/style.css" rel="stylesheet">
<link href="<?php echo esc_url(get_template_directory_uri()); ?>/crm/font/css/font-awesome.min.css" rel="stylesheet">

<!-- Main Angular scripts-->
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/crm/lib/angular/angular.min.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/crm/lib/angular/angular-resource.min.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/crm/lib/angular/sanitize.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/crm/plugins/underscore/underscore-min.js"></script>

<script src="<?php echo esc_url(get_template_directory_uri()); ?>/crm/lib/ui-router/angular-ui-router.min.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/crm/plugins/ui-select/select.min.js"></script>
<link href="<?php echo esc_url(get_template_directory_uri()); ?>/crm/plugins/ui-select/select.min.css" rel="stylesheet">
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/crm/plugins/ngTable/ng-table.min.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/crm/plugins/ngTable/ng-table-export.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/crm/plugins/ngTable/00-directive.js"></script>
<link href="<?php echo esc_url(get_template_directory_uri()); ?>/crm/plugins/ngTable/ng-table.css" rel="stylesheet">
<script
    src="<?php echo esc_url(get_template_directory_uri()); ?>/crm/lib/bootstrap/ui-bootstrap-tpls-0.12.0.min.js"></script>

<script src="<?php echo esc_url(get_template_directory_uri()); ?>/crm/lib/papaparse/papaparse.min.js"></script>
<script
    src="<?php echo esc_url(get_template_directory_uri()); ?>/crm/lib/angular-papa-promise/angular-papa-promise.min.js"></script>


<script src="<?php echo esc_url(get_template_directory_uri()); ?>/crm/app.js"></script>

<!-- Service js -->
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/crm/src/import-price/ImportPriceService.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/crm/src/city-list/CityListService.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/crm/src/city-price/CityPriceService.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/crm/src/key-log/KeyLogService.js"></script>

<!-- Controller js -->
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/crm/src/city-list/CityListController.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/crm/src/city-price/CityPriceController.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/crm/src/import-price/ImportPriceController.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/crm/src/key-log/KeyLogController.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/crm/src/import-price/modal/showItem.js"></script>




</body>
</html>

